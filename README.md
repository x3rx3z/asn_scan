# asn_scan

A multi-process, multi-threaded reverse DNS scanner that operates over entire ASN blocks.

For example, newrelic uses a wildcard service along with wildcard DNS records (perhaps domain fronting).
To scan thier infra (3 ASN blocks);
```
$# time python asn_hosts.py relic_asns | wc -l

98

real	0m24.422s
user	0m26.476s
sys	0m10.832s

```