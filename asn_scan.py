#!/usr/bin/python

import multiprocessing.dummy as mthread
import multiprocessing as mproc
from netaddr import IPNetwork
from subprocess import Popen
import sys, re


# Get ASNs here: https://bgp.he.net/
# Put them into a file, one on each line
# Resolve all the hosts on the ASN: python asn_hosts.py asn_file | tee <tgt>_hosts
# Filter by scope: cat hosts | egrep "scope1|scope2|scope3" | tee inscope_hosts
# Create an in-scope IP list: cat inscope_hosts | cut -d' ' -f3 | tee inscope_ips
# Scan all in-scope hosts: nmap nmap --top-ports 5000 -sV -iL inscope_ips -T4 | tee inscope_nmap

'''
    Print better. Ensures pipes work etc
    Adds newline, flushes output.
'''
def printerr( string, newline=True ):
    if newline:
        sys.stderr.write( string+"\n" )
    else:
        sys.stderr.write( string )
        sys.stderr.flush()

'''
    Print better. Ensures pipes work etc
    Adds newline, flushes output.
'''
def printout( string, newline=True ):
    if newline:
        sys.stdout.write( string+"\n" )
    else:
        sys.stdout.write( string )
        sys.stdout.flush()

'''
    Convert an ASN block to a list of CIDR identifiers
'''
def asn_cidrs( asn ):
    asn = asn.strip("AS")
    cmd = "nmap --script targets-asn --script-args targets-asn.asn="+asn+" 2>/dev/null"
    result = Popen( cmd, shell=True, stdout=-1).communicate()[0].strip()
    cidrs = re.findall(r'[0-9/\.]{10,16}', result)
    return cidrs

'''
    Convert a CIDR identifier to a list of IPs
'''
def cidr_ips( cidr ):
    return [str(ip) for ip in IPNetwork(cidr)]

'''
    Reverse DNS lookup of hostname from IP
'''
def get_host( ip ):
    arpa = '.'.join(ip.split('.')[::-1]) + ".in-addr.arpa"
    result = Popen("dig ptr +short " + arpa, shell=True, stdout=-1).communicate()[0].strip()
    if len(result) > 0:
        printout(ip + " --> " + result[:-1])

'''
    Test a range of CIDRs for open ports (SYN)
    Takes in a CIDR, creates a list of IPs and
    uses multithreading to scan the results.
'''
def scan_cidr( cidr ):
    # Get the list of IPs for this CIDR
    ips = cidr_ips(cidr)
    # Use 1024 threads over IP range (IO bound)
    tpool = mthread.Pool( processes=128 )
    tpool.map( get_host, ips )
    tpool.close()
    tpool.join()

if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Provide a file of ASN identifiers for the target company")
        exit(1)

    asns = set()
    asn_file = sys.argv[1]
    
    # Read in list of cidrs
    with open(asn_file, 'r') as af:
        for asn in af.readlines():
            asns.add(asn.strip('\n'))

    cidrs = []
    for asn in asns:
        cidrs += asn_cidrs(asn)

    # Use all cores over CIDRS
    pool = mproc.Pool()
    pool.map( scan_cidr, cidrs )
    pool.close()
    pool.join()
